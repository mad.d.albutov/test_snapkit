//
//  CustomTextField.swift
//  testSnapKit
//
//  Created by Данил Албутов on 14.12.2021.
//

import UIKit

final class CustomTextField: UITextField {
    private let title: String
    
    init(title: String = "") {
        self.title = title
        super.init(frame: .zero)
        configure()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func configure() {
        let lightGrayColor = UIColor(r: 128, g: 128, b: 128, alpha: 0.6).cgColor
        layer.borderColor = lightGrayColor
        layer.borderWidth = 3.0
        layer.cornerRadius = 30
        placeholder = title
        indent(size: 15)
    }
}
