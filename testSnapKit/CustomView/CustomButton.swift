//
//  CustomButton.swift
//  testSnapKit
//
//  Created by Данил Албутов on 14.12.2021.
//

import UIKit

class CustomButon: UIButton {
    private let title: String
    private let color: UIColor
    
    init(title: String = "", color: UIColor) {
        self.title = title
        self.color = color
        super.init(frame: .zero)
        configure()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func configure() {
        backgroundColor = color
        layer.cornerRadius = 30
        setTitle(title, for: .normal)
        titleLabel?.font = .boldSystemFont(ofSize: 20)
    }
}
