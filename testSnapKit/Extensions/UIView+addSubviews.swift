//
//  UIView+addSubviews.swift
//  testSnapKit
//
//  Created by Данил Албутов on 14.12.2021.
//

import UIKit

extension UIView {
    func addSubviews(_ views: UIView...) {
        views.forEach { [weak self] in
            self?.addSubview($0)
        }
    }
}
