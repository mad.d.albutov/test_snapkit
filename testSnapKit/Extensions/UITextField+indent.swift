//
//  UITextField+indent.swift
//  testSnapKit
//
//  Created by Данил Албутов on 14.12.2021.
//

import UIKit

extension UITextField {
    func indent(size:CGFloat) {
        leftView = UIView(frame: CGRect(x: frame.minX, y: frame.minY, width: size, height: frame.height))
        leftViewMode = .always
    }
}
