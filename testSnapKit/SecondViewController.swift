//
//  SecondViewController.swift
//  testSnapKit
//
//  Created by Данил Албутов on 14.12.2021.
//

import UIKit
import SnapKit

class SecondViewController: UIViewController {
    private let emailTextField = CustomTextField()
    private let passwordTextField = CustomTextField()
    private let loginButton = CustomButon(title: "Go back", color: UIColor(r: 235, g: 0, b: 108, alpha: 1))
    private let infoButton = CustomButon(title: "Show info", color: UIColor(r: 100, g: 100, b: 100, alpha: 1))
    
    private let emailText: String?
    private let passwordText: String?
    
    weak var delegate: DataHandlerDelegate?
    
    init(emailText: String?, passwordText: String?) {
        self.emailText = emailText
        self.passwordText = passwordText
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureViews()
        setupViews()
        setupActions()
    }
    
    @objc func didTapBack() {
        if let previusVC = navigationController?.viewControllers.first as? ViewController {
            delegate = previusVC
        }
        
        delegate?.didDataReciveFromViewController(emailText: emailTextField.text, passwordText: passwordTextField.text)
        navigationController?.popToRootViewController(animated: true)
    }
    
    @objc func didTapInfo() {
        presentModalBottomSheet()
    }
    
    private func presentModalBottomSheet(){
        let vc = ModalBottomSheet(title: "Title", description: fillDescription(text: "text"))
        vc.modalPresentationStyle = .overFullScreen
        present(vc, animated: false, completion: nil)
    }
    
    private func configureViews() {
        view.backgroundColor = .white
        
        emailTextField.text = emailText
        passwordTextField.text = passwordText        
    }
    
    private func setupViews() {
        view.addSubviews(emailTextField, passwordTextField, loginButton, infoButton)
        
        emailTextField.snp.makeConstraints { make in
            make.top.equalTo(view.safeAreaLayoutGuide).offset(30)
            make.left.equalToSuperview().inset(20)
            make.right.equalToSuperview().inset(20)
            make.height.equalTo(60)
        }
        
        passwordTextField.snp.makeConstraints { make in
            make.top.equalTo(emailTextField.snp.bottom).offset(20)
            make.left.equalToSuperview().inset(20)
            make.right.equalToSuperview().inset(20)
            make.height.equalTo(60)
        }
        
        loginButton.snp.makeConstraints { make in
            make.top.equalTo(passwordTextField.snp.bottom).offset(20)
            make.left.equalToSuperview().inset(50)
            make.right.equalToSuperview().inset(50)
            make.height.equalTo(60)
        }
        
        infoButton.snp.makeConstraints { make in
            make.top.equalTo(loginButton.snp.bottom).offset(20)
            make.left.equalToSuperview().inset(50)
            make.right.equalToSuperview().inset(50)
            make.height.equalTo(60)
        }
    }
    
    private func setupActions() {
        loginButton.addTarget(self, action: #selector(didTapBack), for: .touchUpInside)
        infoButton.addTarget(self, action: #selector(didTapInfo), for: .touchUpInside)
    }
    
    private func fillDescription(text: String) -> String{
        var result = ""
        for _ in 0...2050{
            result += " \(text)"
        }
        return result
    }
}
