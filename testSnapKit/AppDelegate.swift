//
//  AppDelegate.swift
//  testSnapKit
//
//  Created by Данил Албутов on 14.12.2021.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        window = UIWindow(frame: UIScreen.main.bounds)
        configViewControllers()
        return true
    }
    
    func configViewControllers() {
        let mainViewController = ViewController(nibName: nil, bundle: nil)
        let navigationController = UINavigationController()
        navigationController.viewControllers.append(mainViewController)
        window?.rootViewController = navigationController
        window?.makeKeyAndVisible()
    }
}

