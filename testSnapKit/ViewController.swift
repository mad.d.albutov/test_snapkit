//
//  ViewController.swift
//  testSnapKit
//
//  Created by Данил Албутов on 14.12.2021.
//

import UIKit
import SnapKit

protocol DataHandlerDelegate: AnyObject{
    func didDataReciveFromViewController(emailText: String?, passwordText: String?)
}

class ViewController: UIViewController {
    private let loginLabel = UILabel()
    private let emailTextField = CustomTextField(title: "Login")
    private let passwordTextField = CustomTextField(title: "Password")
    private let loginButton = CustomButon(title: "Log In", color: UIColor(r: 235, g: 101, b: 108, alpha: 1))
    private let orLabel = UILabel()
    private let facebookLoginButton = CustomButon(title: "Facebook Login", color: UIColor(r: 58, g: 75, b: 140, alpha: 1))
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureViews()
        setupViews()
        setupActions()
    }
    
    @objc func didTapLogin() {
        let secondViewController = SecondViewController(emailText: emailTextField.text,
                                                        passwordText: passwordTextField.text)
        secondViewController.delegate = self
        navigationController?.pushViewController(secondViewController, animated: true)
    }
    
    private func configureViews() {
        view.backgroundColor = .white

        loginLabel.font = UIFont(name: "Gill Sans", size: 40)
        loginLabel.font = UIFont.boldSystemFont(ofSize: 40)
        loginLabel.text = "Log In"
        loginLabel.textColor = UIColor(r: 235, g: 101, b: 108, alpha: 1)
        
        orLabel.text = "OR"        
    }
    
    private func setupViews() {
        view.addSubviews(loginLabel, emailTextField, passwordTextField,
                         loginButton, orLabel, facebookLoginButton)
        loginLabel.snp.makeConstraints { make in
            make.top.equalTo(view.safeAreaLayoutGuide).inset(90)
            make.left.equalTo(view).inset(20)
        }
        
        emailTextField.snp.makeConstraints { make in
            make.top.equalTo(loginLabel.snp.bottom).offset(30)
            make.left.equalToSuperview().inset(20)
            make.right.equalToSuperview().inset(20)
            make.height.equalTo(60)
        }
        
        passwordTextField.snp.makeConstraints { make in
            make.top.equalTo(emailTextField.snp.bottom).offset(20)
            make.left.equalToSuperview().inset(20)
            make.right.equalToSuperview().inset(20)
            make.height.equalTo(60)
        }
        
        loginButton.snp.makeConstraints { make in
            make.top.equalTo(passwordTextField.snp.bottom).offset(20)
            make.left.equalToSuperview().inset(50)
            make.right.equalToSuperview().inset(50)
            make.height.equalTo(60)
        }
        
        orLabel.snp.makeConstraints { make in
            make.top.equalTo(loginButton.snp.bottom).inset(-60)
            make.centerX.equalTo(view)
        }
        
        facebookLoginButton.snp.makeConstraints { make in
            make.top.equalTo(orLabel.snp.bottom).offset(60)
            make.left.equalToSuperview().inset(50)
            make.right.equalToSuperview().inset(50)
            make.height.equalTo(60)
        }
    }
    
    private func setupActions() {
        loginButton.addTarget(self, action: #selector(didTapLogin), for: .touchUpInside)
    }
}

extension ViewController: DataHandlerDelegate{
    func didDataReciveFromViewController(emailText: String? = "", passwordText: String? = "") {
        emailTextField.text = emailText
        passwordTextField.text = passwordText
    }    
}
