//
//  ModalBottomSheet.swift.swift
//  testSnapKit
//
//  Created by Данил Албутов on 16.12.2021.
//

import UIKit
import SnapKit

class ModalBottomSheet: UIViewController {
    private let titleLabel = UILabel()
    private let descriptionLabel = UILabel()
    private let dimmedView = UIView()
    private let contentView = UIView()
    private let scrollView = UIScrollView()
    private let grabberView = UIView()
    
    private var contentViewHeightConstraint: Constraint?
    
    private let customTitle: String
    private let customDescription: String
    
    private let maxDimmedAlpha: CGFloat = 0.7
    private var defaultHeight: CGFloat = 300
    private var currentContainerHeight: CGFloat = 300
    private let dismissibleHeight: CGFloat = 800
    private let maximumContainerHeight: CGFloat = UIScreen.main.bounds.height - 64
    
    init(title: String, description: String) {
        self.customTitle = title
        self.customDescription = description
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        animateShowView()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureViews()
        setupViews()
        setupPanGesture()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        animateDismissView()
    }
    
    private func configureViews() {
        view.backgroundColor = .clear
        view.layer.cornerRadius = 20
        
        dimmedView.backgroundColor = .black        
        dimmedView.translatesAutoresizingMaskIntoConstraints = false
        dimmedView.alpha = 0
        
        contentView.addSubviews(titleLabel, scrollView, grabberView)
        contentView.backgroundColor = .white
        contentView.layer.cornerRadius = 16
        contentView.translatesAutoresizingMaskIntoConstraints = false
        
        grabberView.backgroundColor = UIColor(red: 0.817, green: 0.817, blue: 0.817, alpha: 1)
        grabberView.layer.cornerRadius = 2.5
        
        titleLabel.text = customTitle
        titleLabel.font = UIFont(name: "Gill Sans", size: 18)
        titleLabel.font = .boldSystemFont(ofSize: 18)
        
        descriptionLabel.text = customDescription
        descriptionLabel.lineBreakMode = .byWordWrapping
        descriptionLabel.numberOfLines = 0
        descriptionLabel.font = UIFont(name: "Gill Sans", size: 16)
                
        scrollView.addSubview(descriptionLabel)
        scrollView.contentSize = descriptionLabel.frame.size
    }
    
    private func setupViews() {
        view.backgroundColor = .clear
        view.addSubview(dimmedView)
        view.addSubview(contentView)
        
        dimmedView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        
        contentView.snp.makeConstraints { make in
            make.left.right.bottom.equalToSuperview()
            contentViewHeightConstraint = make.height.equalTo(0).constraint
        }
        
        grabberView.snp.makeConstraints { make in
            make.width.equalTo(36)
            make.height.equalTo(5)
            make.top.equalToSuperview().inset(8)
            make.centerX.equalTo(contentView)
        }
        
        titleLabel.snp.makeConstraints { make in
            make.top.equalToSuperview().inset(37)
            make.left.right.equalToSuperview().inset(16)
        }
        
        scrollView.snp.makeConstraints { make in
            make.top.equalTo(titleLabel.snp.bottom).inset(-16)
            make.bottom.equalTo(contentView.snp.bottom)
            make.right.left.equalToSuperview().inset(16)
        }
        
        descriptionLabel.snp.makeConstraints { make in
            make.edges.equalTo(scrollView)
            make.width.equalTo(scrollView.snp.width)
        }
    }
    
    private func setupPanGesture() {
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(handlePanGesture(gesture:)))
        panGesture.delaysTouchesBegan = false
        panGesture.delaysTouchesEnded = false
        view.addGestureRecognizer(panGesture)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleTap(_:)))
        dimmedView.addGestureRecognizer(tap)
    }
    
    @objc private func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        animateDismissView()
    }
    
    @objc private func handlePanGesture(gesture: UIPanGestureRecognizer){
        let translation = gesture.translation(in: view)
        let newHeight = currentContainerHeight - translation.y
        
        switch gesture.state {
        case .changed:
            setContentViewHeight(newHeight: newHeight)
        case .ended:
            setAnimationOptionForContent(newHeight: newHeight, translation: translation)
        default:
            break
        }
    }
    
    private func setDefaultContentHeight() {
        let totalOffset: CGFloat = 100
        let totalHeight = totalOffset + titleLabel.frame.height + descriptionLabel.frame.height
        defaultHeight = descriptionLabel.frame.height > maximumContainerHeight ? maximumContainerHeight : totalHeight
    }
    
    private func setContentViewHeight(newHeight: CGFloat) {
        if newHeight < maximumContainerHeight {
            contentViewHeightConstraint?.update(offset: newHeight)
            view.layoutIfNeeded()
        }
    }
    
    private func setAnimationOptionForContent(newHeight: CGFloat, translation: CGPoint){
        let isDraggingDown = translation.y > 0
        if newHeight < dismissibleHeight && translation.y > 50 {
            animateDismissView()
        } else if newHeight < defaultHeight {
            animateContentHeight(defaultHeight)
        } else if newHeight < maximumContainerHeight && isDraggingDown {
            animateContentHeight(defaultHeight)
        } else if newHeight > defaultHeight && !isDraggingDown {
            animateContentHeight(maximumContainerHeight)
        }
    }
    
    private func animateContentHeight(_ height: CGFloat) {
        UIView.animate(withDuration: 0.4) { [weak self] in
            self?.contentViewHeightConstraint?.update(offset: height)
            self?.view.layoutIfNeeded()
        }
        
        currentContainerHeight = height
    }
    
    private func animateDismissView() {
        animateContentViewHeight()
        animateDimmerViewAlpha()
    }
    
    private func animateContentViewHeight() {
        UIView.animate(withDuration: 0.3) { [weak self] in
            self?.contentViewHeightConstraint?.update(offset: 0)
            self?.view.layoutIfNeeded()
        }
    }
    
    private func animateDimmerViewAlpha() {
        dimmedView.alpha = maxDimmedAlpha
        
        UIView.animate(withDuration: 0.3) { [weak self] in
            self?.dimmedView.alpha = 0
        } completion: { [weak self] _ in
            self?.dismiss(animated: false)
        }
    }
    
    private func animateShowView() {
        dimmedView.alpha = 0
        
        UIView.animate(withDuration: 0.3) { [weak self] in
            self?.dimmedView.alpha = self?.maxDimmedAlpha ?? 0
            UIView.animate(withDuration: 0.3) { [weak self] in
                guard let self = self else {
                    return
                }
                
                self.setDefaultContentHeight()
                self.currentContainerHeight = self.defaultHeight
                self.contentViewHeightConstraint?.update(offset: self.defaultHeight)
                self.view.layoutIfNeeded()
            }
        }
    }
}

