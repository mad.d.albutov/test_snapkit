//
//  UIColor.swift
//  testSnapKit
//
//  Created by Данил Албутов on 14.12.

import UIKit

extension UIColor {
    convenience init(r: CGFloat, g: CGFloat, b: CGFloat, alpha: CGFloat) {
        init(red: r / 255, green: g / 255, blue: b / 255, alpha: alpha)
    }
}
